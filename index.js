let arr =  ['hello', 'world', 33, '33', null];

function filterBy(array, type){
    return array.filter(element => typeof(element) !== type);
}
console.log(filterBy(arr, 'string'));
